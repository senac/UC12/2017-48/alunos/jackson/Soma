package br.com.senac.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SomaServlet extends HttpServlet {

    @Override
    protected void doGet(
            HttpServletRequest requisicao,
            HttpServletResponse resposta) throws ServletException, IOException {
        String param1 = requisicao.getParameter("numero1");
        int numero1 = Integer.parseInt(param1);
        
        String param2 = requisicao.getParameter("numero2") ; 
        int numero2 = Integer.parseInt(param2) ; 
        
        int resultado = numero1 + numero2 ; 
        
        PrintWriter saida =  resposta.getWriter() ; 
        
        saida.print("<html>");
        saida.print("<body>");
        saida.print("O Resultado é " + resultado);
        saida.print("</body>");
        saida.print("</html>");

    }

}
